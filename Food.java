public class Food
{
    private String description;
    private int caloreies;
    
    public int getCalories()
    {
        return calories;
    }
    
    public void setCalories(int inCalories)
    {
        calories = in Calories;
    }
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * Set the description
     * @param String inDescrpition: The new descrpition
     */
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    
    @Override
    public String toString()
    {
        return "somebody brought " + getDescription();
    }
    
}